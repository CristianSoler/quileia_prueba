package com.example.serviciosmedicos;

import android.support.multidex.MultiDexApplication;

import com.example.serviciosmedicos.di.components.ApplicationComponent;
import com.example.serviciosmedicos.di.components.DaggerApplicationComponent;
import com.example.serviciosmedicos.di.modules.ApplicationModule;

public class ServicesApplication extends MultiDexApplication {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeInjector();
        setUpInjection();
    }

    private void initializeInjector(){
        this.applicationComponent = DaggerApplicationComponent.builder().build();
                //.applicationModule(new ApplicationModule(this)).build();
    }

    private void setUpInjection(){
        getApplicationComponent().inject(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}

package com.example.serviciosmedicos.repository.remote;

import com.example.serviciosmedicos.model.doctor.Doctor;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiServices {

    @GET("doctors/")
    Call<List<Doctor>> getAllPosts();

    @GET("doctors/{id}")
    Call<Doctor> getSpecificDoctor(@Path("id") int id);

    @POST("doctors/{id}")
    Call<Doctor> editDoctor(@Path("id") int id, @Body Doctor doctor);
}

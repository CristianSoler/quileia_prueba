package com.example.serviciosmedicos.repository.remote;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiCaller {

    public ApiCaller() {
    }

    public void setUpBuilder(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public void sendData(List<Object> data){

    }
}

package com.example.serviciosmedicos.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.serviciosmedicos.model.patient.Patient;
import com.example.serviciosmedicos.repository.local.MedicalServicesDatabase;
import com.example.serviciosmedicos.repository.local.dao.PatientDao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PatientRepository {

    private PatientDao patientDao;
    private LiveData<List<Patient>> allPatient;

    @Inject
    public PatientRepository(Application application){
        MedicalServicesDatabase db = MedicalServicesDatabase.getInstance(application);
        patientDao = db.patientDao();
        allPatient = patientDao.getAllPatients();
    }

    public void insertPatient(Patient patient){
        new PatientRepository.InsertPatientAsyncTask(patientDao).execute(patient);
    }

    public void updatePatient(Patient patient){
        new PatientRepository.UpdatePatientAsyncTask(patientDao).execute(patient);
    }

    public void deletePatient(Patient patient){
        new PatientRepository.DeletePatientAsyncTask(patientDao).execute(patient);
    }

    public void deleteAllPatients(){
        new PatientRepository.DeleteAllPatientAsyncTask(patientDao).execute();
    }

    public LiveData<List<Patient>> getAllPatient(){
        return allPatient;
    }

    private static class InsertPatientAsyncTask extends AsyncTask<Patient, Void, Void> {


        private PatientDao patientDao;

        private InsertPatientAsyncTask(PatientDao patientDao) {
            this.patientDao = patientDao;
        }

        @Override
        protected Void doInBackground(Patient... patients) {
            patientDao.insert(patients[0]);
            return null;
        }
    }

    private static class UpdatePatientAsyncTask extends AsyncTask<Patient, Void, Void> {


        private PatientDao patientDao;

        private UpdatePatientAsyncTask(PatientDao patientDao) {
            this.patientDao = patientDao;
        }

        @Override
        protected Void doInBackground(Patient... patients) {
            patientDao.update(patients[0]);
            return null;
        }
    }

    private static class DeletePatientAsyncTask extends AsyncTask<Patient, Void, Void> {


        private PatientDao patientDao;

        private DeletePatientAsyncTask(PatientDao patientDao) {
            this.patientDao = patientDao;
        }

        @Override
        protected Void doInBackground(Patient... patients) {
            patientDao.delete(patients[0]);
            return null;
        }
    }

    private static class DeleteAllPatientAsyncTask extends AsyncTask<Void, Void, Void> {


        private PatientDao patientDao;

        private DeleteAllPatientAsyncTask(PatientDao patientDao) {
            this.patientDao = patientDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            patientDao.deleteAllPatients();
            return null;
        }
    }

}

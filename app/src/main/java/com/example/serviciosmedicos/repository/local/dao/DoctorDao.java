package com.example.serviciosmedicos.repository.local.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.serviciosmedicos.model.doctor.Doctor;

import java.util.List;

@Dao
public interface DoctorDao {

    @Insert
    void insert(Doctor doctor);

    @Update
    void update(Doctor doctor);

    @Delete
    void delete(Doctor doctor);



    @Query("DELETE FROM doctor_table")
    void deleteAllDoctors();

    @Query("SELECT * FROM doctor_table")
    LiveData<List<Doctor>> getAllDoctors();

    @Query("SELECT * FROM doctor_table WHERE professionalCard = :idDoctor")
    Doctor getSpecificDoctor(String idDoctor);
}

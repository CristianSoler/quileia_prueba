package com.example.serviciosmedicos.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.serviciosmedicos.model.doctor.Doctor;
import com.example.serviciosmedicos.repository.local.MedicalServicesDatabase;
import com.example.serviciosmedicos.repository.local.dao.DoctorDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DoctorRepository {

    private DoctorDao doctorDao;
    private LiveData<List<Doctor>> allDoctor;

    @Inject
    public DoctorRepository(Application application){
        MedicalServicesDatabase db = MedicalServicesDatabase.getInstance(application);
        doctorDao = db.doctorDao();
        allDoctor = doctorDao.getAllDoctors();
    }

    public void insertDoctor(Doctor doctor){
        new InsertDoctorAsyncTask(doctorDao).execute(doctor);
    }

    public void updateDoctor(Doctor doctor){
        new UpdateDoctorAsyncTask(doctorDao).execute(doctor);
    }

    public void deleteDoctor(Doctor doctor){
        new DeleteDoctorAsyncTask(doctorDao).execute(doctor);
    }

    public void deleteAllDoctors(){
        new DeleteAllDoctorAsyncTask(doctorDao).execute();
    }

    public LiveData<List<Doctor>> getAllDoctor(){
        return allDoctor;
    }

    public Doctor getSpecificDoctor(String doctorID) throws ExecutionException, InterruptedException {
        GetSpecificDoctor specificDoctor = new GetSpecificDoctor(doctorDao, doctorID);
        specificDoctor.execute();
        return specificDoctor.get();
    }

    private static class InsertDoctorAsyncTask extends AsyncTask<Doctor, Void, Void> {


        private DoctorDao doctorDao;

        private InsertDoctorAsyncTask(DoctorDao doctorDao) {
            this.doctorDao = doctorDao;
        }

        @Override
        protected Void doInBackground(Doctor... doctors) {
            doctorDao.insert(doctors[0]);
            return null;
        }
    }

    private static class UpdateDoctorAsyncTask extends AsyncTask<Doctor, Void, Void> {


        private DoctorDao doctorDao;

        private UpdateDoctorAsyncTask(DoctorDao doctorDao) {
            this.doctorDao = doctorDao;
        }

        @Override
        protected Void doInBackground(Doctor... doctors) {
            doctorDao.update(doctors[0]);
            return null;
        }
    }

    private static class DeleteDoctorAsyncTask extends AsyncTask<Doctor, Void, Void> {


        private DoctorDao doctorDao;

        private DeleteDoctorAsyncTask(DoctorDao doctorDao) {
            this.doctorDao = doctorDao;
        }

        @Override
        protected Void doInBackground(Doctor... doctors) {
            doctorDao.delete(doctors[0]);
            return null;
        }
    }

    private static class DeleteAllDoctorAsyncTask extends AsyncTask<Void, Void, Void> {


        private DoctorDao doctorDao;

        private DeleteAllDoctorAsyncTask(DoctorDao doctorDao) {
            this.doctorDao = doctorDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            doctorDao.deleteAllDoctors();
            return null;
        }
    }

    private static class GetSpecificDoctor extends AsyncTask<Void, Void, Doctor>{

        private DoctorDao doctorDao;
        private String doctorID;

        private GetSpecificDoctor(DoctorDao doctorDao, String doctorID) {
            this.doctorDao = doctorDao;
            this.doctorID = doctorID;
        }

        @Override
        protected Doctor doInBackground(Void... voids) {
            return doctorDao.getSpecificDoctor(doctorID);
        }
    }
}

package com.example.serviciosmedicos.repository.local;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.example.serviciosmedicos.model.appointment.MedicalAppointment;
import com.example.serviciosmedicos.model.doctor.Doctor;
import com.example.serviciosmedicos.model.patient.Patient;
import com.example.serviciosmedicos.repository.local.dao.DoctorDao;
import com.example.serviciosmedicos.repository.local.dao.MedicalAppointmentDao;
import com.example.serviciosmedicos.repository.local.dao.PatientDao;

@Database(entities = {Patient.class, Doctor.class, MedicalAppointment.class}, version = 1)
public abstract class MedicalServicesDatabase extends RoomDatabase {


    private static MedicalServicesDatabase instance;

    public abstract PatientDao patientDao();
    public abstract DoctorDao doctorDao();
    public abstract MedicalAppointmentDao medicalAppointmentDao();

    public static synchronized MedicalServicesDatabase getInstance(Context context){
        if(instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), MedicalServicesDatabase.class, "medical_database")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
        }
    };


}

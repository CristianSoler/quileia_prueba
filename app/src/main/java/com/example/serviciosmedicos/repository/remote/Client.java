package com.example.serviciosmedicos.repository.remote;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.example.serviciosmedicos.model.doctor.Doctor;
import com.example.serviciosmedicos.model.patient.Patient;
import com.example.serviciosmedicos.utils.Constants;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Client extends AsyncTask<Void, Void, String> {

    String response = "";
    Context context;
    public Client(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(Void... voids) {
        Socket socket;
        try {
                socket = new Socket("192.168.1.65", 5000);//Direccion ip del servidor (local)
                ObjectOutputStream os = new ObjectOutputStream(socket.getOutputStream());

                ArrayList<Object> sync = new ArrayList<>();

                //sync.add(getAllDoctor());
                //sync.add(getAllPatient());

                os.writeObject(sync);
                socket.close();
        } catch (IOException ex) {
            Log.e("TAG","Cliente> " + ex.getMessage());
        }
        return response;
    }

    /**
    public List<Doctor> getAllDoctor(){
        SQLiteBuilder connection = new SQLiteBuilder(context);
        SQLiteDatabase db = connection.getReadableDatabase();
        String[] shields = {Constants.PROFESIONAL_CARD_SHIELD, Constants.NAME_DOCTOR_SHIELD, Constants.SPECIALITY_SHIELD, Constants.EXPERIENCE_SHIELD, Constants.CONSULTING_ROOM_SHIELD, Constants.DOMICILE_SHIELD};
        Cursor cursor = db.query(Constants.DOCTOR, shields,null,null,null,null,null);

        ArrayList<Doctor> doctors= new ArrayList<>();
        while(cursor.moveToNext()){
            doctors.add(new Doctor(
                    cursor.getString(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getFloat(3),
                    cursor.getString(4),
                    (cursor.getInt(5) > 0)
            ));
        }
        return doctors;
    }

    public List<Patient> getAllPatient(){
        SQLiteBuilder connection = new SQLiteBuilder(context);
        SQLiteDatabase db = connection.getReadableDatabase();
        String[] shields = {Constants.IDENTIFICATION_SHIELD, Constants.NAME_SHIELD, Constants.LAST_NAME_SHIELD, Constants.BIRTHDATE_SHIELD, Constants.MEDICATION_SHIELD};
        Cursor cursor = db.query(Constants.PATIENT, shields,null,null,null,null,null);
        ArrayList<Patient> patients= new ArrayList<>();
        while(cursor.moveToNext()){
            Date date = new Date(cursor.getLong(3));
            patients.add(new Patient(
                    cursor.getString(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    date,
                    (cursor.getInt(4) > 0))
            );
        }
        return patients;
    }
    */
}

package com.example.serviciosmedicos.repository.local.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.serviciosmedicos.model.appointment.MedicalAppointment;

import java.util.List;

@Dao
public interface MedicalAppointmentDao {
    @Insert
    void insert(MedicalAppointment medicalAppointment);

    @Update
    void update(MedicalAppointment medicalAppointment);

    @Delete
    void delete(MedicalAppointment medicalAppointment);



    @Query("DELETE FROM medical_appointment_table")
    void deleteAllMedicalAppointments();

    @Query("SELECT * FROM medical_appointment_table")
    LiveData<List<MedicalAppointment>> getAllMedicalAppointments();

    @Query("SELECT * FROM medical_appointment_table WHERE idDoctor = :profesionalCardDoctor")
    List<MedicalAppointment> getSpecificMedicalAppointmentWithDoctorID(String profesionalCardDoctor);

    @Query("SELECT * FROM medical_appointment_table WHERE idPatient = :identificationPatient")
    LiveData<List<MedicalAppointment>> getSpecificMedicalAppointmentWithPatientID(String identificationPatient);

    @Query("DELETE FROM medical_appointment_table WHERE idPatient = :identificationPatient")
    void deleteMedicalAppointmentFromSpecificPatient(String identificationPatient);

    @Query("DELETE FROM medical_appointment_table WHERE idDoctor = :profesionalCardDoctor")
    void deleteMedicalAppointmentFromSpecificDoctor(String profesionalCardDoctor);

    @Query("SELECT* FROM medical_appointment_table WHERE id = :idAppointment")
    MedicalAppointment getSpecificMedicalAppointment(Integer idAppointment);
}

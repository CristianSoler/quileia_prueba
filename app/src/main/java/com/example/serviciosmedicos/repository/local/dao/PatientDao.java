package com.example.serviciosmedicos.repository.local.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.serviciosmedicos.model.patient.Patient;

import java.util.List;

@Dao
public interface PatientDao {

    @Insert
    void insert(Patient patient);

    @Update
    void update(Patient patient);

    @Delete
    void delete(Patient patient);



    @Query("DELETE FROM patient_table")
    void deleteAllPatients();

    @Query("SELECT * FROM patient_table")
    LiveData<List<Patient>> getAllPatients();

}

package com.example.serviciosmedicos.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.os.AsyncTask;
import android.support.annotation.Nullable;

import com.example.serviciosmedicos.model.appointment.MedicalAppointment;
import com.example.serviciosmedicos.repository.local.MedicalServicesDatabase;
import com.example.serviciosmedicos.repository.local.dao.MedicalAppointmentDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MedicalAppointmentRepository {

    private MedicalAppointmentDao medicalAppointmentDao;
    private LiveData<List<MedicalAppointment>> allMedicalAppointment;

    @Inject
    public MedicalAppointmentRepository(Application application){
        MedicalServicesDatabase db = MedicalServicesDatabase.getInstance(application);
        medicalAppointmentDao = db.medicalAppointmentDao();
        allMedicalAppointment = medicalAppointmentDao.getAllMedicalAppointments();
    }

    public void insertMedicalAppointmet(MedicalAppointment medicalAppointment){
        new MedicalAppointmentRepository.InsertMedicalAppointmentAsyncTask(medicalAppointmentDao).execute(medicalAppointment);
    }

    public void updateMedicalAppointmet(MedicalAppointment medicalAppointment){
        new MedicalAppointmentRepository.UpdateMedicalAppointmentAsyncTask(medicalAppointmentDao).execute(medicalAppointment);
    }

    public void deleteMedicalAppointmet(MedicalAppointment medicalAppointment){
        new MedicalAppointmentRepository.DeleteMedicalAppointmentAsyncTask(medicalAppointmentDao).execute(medicalAppointment);
    }

    public void deleteAllMedicalAppointmets(){
        new MedicalAppointmentRepository.DeleteAllMedicalAppointmentAsyncTask(medicalAppointmentDao).execute();
    }

    public LiveData<List<MedicalAppointment>> getAllMedicalAppointment(){
        return allMedicalAppointment;
    }

    public List<MedicalAppointment> getSpecificMedicalAppointmentWithDoctorID(String doctorID) throws ExecutionException, InterruptedException {
        MedicalAppointmentRepository.GetSpecificMedicalAppointmentWithDoctorID doctorAppointment = new MedicalAppointmentRepository.GetSpecificMedicalAppointmentWithDoctorID(medicalAppointmentDao, doctorID);
        doctorAppointment.execute();
        return doctorAppointment.get();
    }

    public LiveData<List<MedicalAppointment>> getSpecificMedicalAppointmentWithPatientID(String patientID) throws ExecutionException, InterruptedException {
        MedicalAppointmentRepository.GetSpecificMedicalAppointmentWithPatientID patientAppointment = new MedicalAppointmentRepository.GetSpecificMedicalAppointmentWithPatientID(medicalAppointmentDao, patientID);
        patientAppointment.execute();
        return patientAppointment.get();
    }

    public MedicalAppointment getSpecificMedicalAppointment(Integer appointmentID) throws ExecutionException, InterruptedException {
        GetSpecificMedicalAppointment medicalAppointment = new GetSpecificMedicalAppointment(medicalAppointmentDao,appointmentID);
        medicalAppointment.execute();
        return medicalAppointment.get();
    }

    public void deleteMedicalAppointmentFromSpecificPatient(String patientID){
        new MedicalAppointmentRepository.DeleteMedicalAppointmentFromSpecificPatient(medicalAppointmentDao, patientID).execute();
    }

    public void deleteMedicalAppointmentFromSpecificDoctor(String doctorID){
        new MedicalAppointmentRepository.DeleteMedicalAppointmentFromSpecificDoctor(medicalAppointmentDao, doctorID).execute();
    }

    private static class InsertMedicalAppointmentAsyncTask extends AsyncTask<MedicalAppointment, Void, Void> {

        private MedicalAppointmentDao medicalAppointmentDao;

        private InsertMedicalAppointmentAsyncTask(MedicalAppointmentDao medicalAppointmentDao) {
            this.medicalAppointmentDao = medicalAppointmentDao;
        }

        @Override
        protected Void doInBackground(MedicalAppointment... appointments) {
            medicalAppointmentDao.insert(appointments[0]);
            return null;
        }
    }

    private static class UpdateMedicalAppointmentAsyncTask extends AsyncTask<MedicalAppointment, Void, Void> {


        private MedicalAppointmentDao medicalAppointmentDao;

        private UpdateMedicalAppointmentAsyncTask(MedicalAppointmentDao medicalAppointmentDao) {
            this.medicalAppointmentDao = medicalAppointmentDao;
        }

        @Override
        protected Void doInBackground(MedicalAppointment... appointments) {
            medicalAppointmentDao.update(appointments[0]);
            return null;
        }
    }

    private static class DeleteMedicalAppointmentAsyncTask extends AsyncTask<MedicalAppointment, Void, Void> {


        private MedicalAppointmentDao medicalAppointmentDao;

        private DeleteMedicalAppointmentAsyncTask(MedicalAppointmentDao medicalAppointmentDao) {
            this.medicalAppointmentDao = medicalAppointmentDao;
        }

        @Override
        protected Void doInBackground(MedicalAppointment... appointments) {
            medicalAppointmentDao.delete(appointments[0]);
            return null;
        }
    }

    private static class DeleteAllMedicalAppointmentAsyncTask extends AsyncTask<Void, Void, Void> {


        private MedicalAppointmentDao medicalAppointmentDao;

        private DeleteAllMedicalAppointmentAsyncTask(MedicalAppointmentDao medicalAppointmentDao) {
            this.medicalAppointmentDao = medicalAppointmentDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            medicalAppointmentDao.deleteAllMedicalAppointments();
            return null;
        }
    }

    private static class GetSpecificMedicalAppointmentWithDoctorID extends AsyncTask<Void, Void, List<MedicalAppointment>>{

        private MedicalAppointmentDao medicalAppointmentDao;
        private String doctorID;

        private GetSpecificMedicalAppointmentWithDoctorID(MedicalAppointmentDao medicalAppointmentDao, String doctorID) {
            this.medicalAppointmentDao = medicalAppointmentDao;
            this.doctorID = doctorID;
        }

        @Override
        protected List<MedicalAppointment> doInBackground(Void... voids) {
            return medicalAppointmentDao.getSpecificMedicalAppointmentWithDoctorID(doctorID);
        }
    }

    private static class GetSpecificMedicalAppointmentWithPatientID extends AsyncTask<Void, Void, LiveData<List<MedicalAppointment>>>{

        private MedicalAppointmentDao medicalAppointmentDao;
        private String patientID;

        private GetSpecificMedicalAppointmentWithPatientID(MedicalAppointmentDao medicalAppointmentDao, String patientID) {
            this.medicalAppointmentDao = medicalAppointmentDao;
            this.patientID = patientID;
        }

        @Override
        protected LiveData<List<MedicalAppointment>> doInBackground(Void... voids) {
            return medicalAppointmentDao.getSpecificMedicalAppointmentWithPatientID(patientID);
        }
    }

    private static class DeleteMedicalAppointmentFromSpecificPatient extends AsyncTask<Void, Void, Void>{

        private MedicalAppointmentDao medicalAppointmentDao;
        private String patientID;

        private DeleteMedicalAppointmentFromSpecificPatient(MedicalAppointmentDao medicalAppointmentDao, String patientID) {
            this.medicalAppointmentDao = medicalAppointmentDao;
            this.patientID = patientID;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            medicalAppointmentDao.deleteMedicalAppointmentFromSpecificPatient(patientID);
            return null;
        }
    }

    private static class DeleteMedicalAppointmentFromSpecificDoctor extends AsyncTask<Void, Void, Void> {

        private MedicalAppointmentDao medicalAppointmentDao;
        private String doctorID;

        public DeleteMedicalAppointmentFromSpecificDoctor(MedicalAppointmentDao medicalAppointmentDao, String doctorID) {
            this.medicalAppointmentDao = medicalAppointmentDao;
            this.doctorID = doctorID;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            medicalAppointmentDao.deleteMedicalAppointmentFromSpecificDoctor(doctorID);
            return null;
        }
    }

    public static class GetSpecificMedicalAppointment extends AsyncTask<Void, Void, MedicalAppointment>{

        private MedicalAppointmentDao medicalAppointmentDao;
        private Integer medicalAppointmentID;

        public GetSpecificMedicalAppointment(MedicalAppointmentDao medicalAppointmentDao, Integer medicalAppointmentID) {
            this.medicalAppointmentDao = medicalAppointmentDao;
            this.medicalAppointmentID = medicalAppointmentID;
        }

        @Override
        protected MedicalAppointment doInBackground(Void... voids) {
            return medicalAppointmentDao.getSpecificMedicalAppointment(medicalAppointmentID);
        }
    }
}

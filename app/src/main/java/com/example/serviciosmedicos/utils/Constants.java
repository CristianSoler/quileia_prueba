package com.example.serviciosmedicos.utils;

public class Constants {

    private Constants() {
    }

    //Local database info
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "medic.db";

    //Doctor table
    public static final String DOCTOR = "doctor";
    public static final String NAME_DOCTOR_SHIELD = "name";
    public static final String PROFESIONAL_CARD_SHIELD = "profesional_card";
    public static final String SPECIALITY_SHIELD = "speciality";
    public static final String EXPERIENCE_SHIELD = "experience";
    public static final String CONSULTING_ROOM_SHIELD = "consulting_room";
    public static final String DOMICILE_SHIELD = "domicile";
    public static final String CREATE_DOCTOR_TABLE = "CREATE TABLE "+DOCTOR +"("+ PROFESIONAL_CARD_SHIELD +" STRING PRIMARY KEY NOT NULL, "+ NAME_DOCTOR_SHIELD + " STRING, "+ SPECIALITY_SHIELD +" STRING,"+ EXPERIENCE_SHIELD +" FLOAT,"+ CONSULTING_ROOM_SHIELD +" STRING,"+ DOMICILE_SHIELD +" BOOLEAN)";



    //Patient table
    public static final String PATIENT = "patient";
    public static final String IDENTIFICATION_SHIELD = "identification";
    public static final String NAME_SHIELD = "name";
    public static final String LAST_NAME_SHIELD = "last_name";
    public static final String BIRTHDATE_SHIELD = "birthdate";
    public static final String MEDICATION_SHIELD = "medication";
    public static final String CREATE_PATIENT_TABLE = "CREATE TABLE " + PATIENT +" ("+ IDENTIFICATION_SHIELD +" STRING PRIMARY KEY NOT NULL, "+ NAME_SHIELD +" STRING, "+ LAST_NAME_SHIELD +" STRING, "+ BIRTHDATE_SHIELD +" LONG, "+ MEDICATION_SHIELD +" BOOLEAN)";


    //Medical Appointment table
    public static final String MEDICAL_APPOINTMENT = "medical_appointment";
    public static final String ID_SHIELD = "id";
    public static final String ID_PATIENT_SHIELD = "id_patient";
    public static final String ID_DOCTOR_SHIELD = "id_doctor";
    public static final String STATE_SHIELD = "state";
    public static final String APPOINTMENT_DATE_SHIELD = "appointment_date";
    public static final String FEE_VALUE_SHIELD = "fee_value";
    public static final String ARRIVED_DATE_SHIELD ="arrived_date";
    public static final String PATIENT_SIGNATURE_SHIELD = "patient_signature";
    public static final String CREATE_MEDICAL_TABLE = "CREATE TABLE "+ MEDICAL_APPOINTMENT +" ("+ ID_SHIELD +" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+ ID_PATIENT_SHIELD +" STRING, "+ ID_DOCTOR_SHIELD +" STRING, "+ STATE_SHIELD +" INTEGER, "+ APPOINTMENT_DATE_SHIELD +" LONG, "+ FEE_VALUE_SHIELD +" DOUBLE, "+ ARRIVED_DATE_SHIELD +" LONG, "+ PATIENT_SIGNATURE_SHIELD +" BYTE[])";

}

package com.example.serviciosmedicos.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
    private Utils() {
    }

    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }

    public static Bitmap getImage(byte[] data){
        return BitmapFactory.decodeByteArray(data,0, data.length);
    }

    public static String getDateFormat(Date date){
        SimpleDateFormat textFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        return textFormat.format(date);
    }

    public static Date getDate(Long longDate){
        Date date = new Date(longDate);
        return date;
    }

}

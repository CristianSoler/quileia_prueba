package com.example.serviciosmedicos.ui.doctor.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.example.serviciosmedicos.model.appointment.MedicalAppointment;
import com.example.serviciosmedicos.model.doctor.Doctor;
import com.example.serviciosmedicos.repository.DoctorRepository;
import com.example.serviciosmedicos.repository.MedicalAppointmentRepository;

import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class HistoryDoctorViewModel extends AndroidViewModel {

    private DoctorRepository doctorRepository;
    private MedicalAppointmentRepository medicalAppointmentRepository;

    @Inject
    public HistoryDoctorViewModel(@NonNull Application application) {
        super(application);
        doctorRepository = new DoctorRepository(application);
        medicalAppointmentRepository = new MedicalAppointmentRepository(application);
    }

    public void updateDoctor(Doctor doctor){
        doctorRepository.updateDoctor(doctor);
    }

    public void deleteDoctor(Doctor doctor){
        doctorRepository.deleteDoctor(doctor);
        deleteMedicalAppointmentFromSpecificDoctor(doctor.getProfessionalCard());
    }

    public void deleteMedicalAppointmentFromSpecificDoctor(String doctorID){
        medicalAppointmentRepository.deleteMedicalAppointmentFromSpecificDoctor(doctorID);
    }

    public int isAvailableMedicalAppointment(String doctorID) throws ExecutionException, InterruptedException {
        int state = -1;
        List<MedicalAppointment> medicalAppointmentList = medicalAppointmentRepository.getSpecificMedicalAppointmentWithDoctorID(doctorID);

        for(MedicalAppointment medicalAppointment : medicalAppointmentList) {
            if (medicalAppointment.getState() == 0){
                state = 0;
            }else if ((medicalAppointment.getState()==1) || (medicalAppointment.getState()==2) || (medicalAppointment.getState() ==3)){
                state = 1;
            }
        }

        return state;
    }

    public List<MedicalAppointment> getSpecificAppointmentDoctor(String doctorID) throws ExecutionException, InterruptedException {
        List<MedicalAppointment> medicalAppointmentList = medicalAppointmentRepository.getSpecificMedicalAppointmentWithDoctorID(doctorID);
        System.out.println(medicalAppointmentList.size());
        return medicalAppointmentList;
    }

}

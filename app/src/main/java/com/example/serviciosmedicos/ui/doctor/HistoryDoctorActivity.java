package com.example.serviciosmedicos.ui.doctor;

import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.serviciosmedicos.R;
import com.example.serviciosmedicos.ServicesApplication;
import com.example.serviciosmedicos.model.adapter.doctor.AdapterHistoryDoctor;
import com.example.serviciosmedicos.model.doctor.Doctor;
import com.example.serviciosmedicos.ui.doctor.viewModel.HistoryDoctorViewModel;

import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HistoryDoctorActivity extends AppCompatActivity{

    private HistoryDoctorViewModel historyDoctorViewModel;
    Doctor doctor;

    @BindView(R.id.toolbar_history_doctor)
    Toolbar toolbar;

    @BindView(R.id.history_doctor_list)
    RecyclerView historyDoctorList;

    @BindView(R.id.profesional_card_info)
    TextView profesionalCard;

    @BindView(R.id.name_doctor_info)
    TextView name;

    @BindView(R.id.speciality_info)
    TextInputEditText speciality;

    @BindView(R.id.experience_info)
    TextInputEditText experience;

    @BindView(R.id.consulting_room_info)
    TextInputEditText consultingRoom;

    @BindView(R.id.domicile_info)
    Spinner domicile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_doctor);
        ButterKnife.bind(this);
        setUpInjection();
        setUpToolbar();
        doctor = (Doctor) getIntent().getSerializableExtra("Doctor");
        historyDoctorViewModel = ViewModelProviders.of(this).get(HistoryDoctorViewModel.class);
        showInfoDoctor();
        showDoctorHistory();
    }

    private void setUpInjection() {
        ((ServicesApplication) getApplication()).getApplicationComponent().inject(this);
    }

    public void setUpToolbar() {
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.details));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
            case R.id.action_delete: {
                try {
                    deleteAvailableAppointment();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            }
            case R.id.action_save: {
                historyDoctorViewModel.updateDoctor(getDoctorData());
                hideKeyboard();
                setEditDisabled();
                break;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public void deleteAvailableAppointment() throws ExecutionException, InterruptedException {
        if (historyDoctorViewModel.isAvailableMedicalAppointment(doctor.getProfessionalCard()) == 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.cannot_delete_title))
                    .setTitle(getString(R.string.cannot_delete_description));
            builder.setPositiveButton(getString(R.string.understood), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }else if(historyDoctorViewModel.isAvailableMedicalAppointment(doctor.getProfessionalCard()) == 1){
            //Pedir confirmacion en un dialog, si acepta se borra en cascada
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.question_delete_title))
                    .setTitle(getString(R.string.question_delete_description));
            builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    historyDoctorViewModel.deleteDoctor(getDoctorData());
                    finish();
                }
            }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }else{
            historyDoctorViewModel.deleteDoctor(getDoctorData());
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.details_option_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void showInfoDoctor() {
        profesionalCard.setText(doctor.getProfessionalCard());
        name.setText(doctor.getName());
        speciality.setText(doctor.getSpeciality());
        experience.setText(String.valueOf(doctor.getExperience()));
        consultingRoom.setText(doctor.getConsultingRoom());
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.domicile_options, R.layout.spinner_center_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        domicile.setAdapter(adapter);

        if (doctor.isDomicile()) {
            domicile.setSelection(0);
        } else {
            domicile.setSelection(1);
        }


    }

    @OnClick(R.id.btn_edit_consulting_room)
    public void editConsultingRoom() {
        consultingRoom.setEnabled(true);
    }

    @OnClick(R.id.btn_edit_experience)
    public void editExperience() {
        experience.setEnabled(true);
    }

    @OnClick(R.id.btn_edit_speciality)
    public void editSpeciality() {
        speciality.setEnabled(true);
    }

    public Doctor getDoctorData() {
        return  new Doctor(
                profesionalCard.getText().toString(),
                name.getText().toString(),
                speciality.getText().toString(),
                Float.valueOf(experience.getText().toString()),
                consultingRoom.getText().toString(),
                domicile.getSelectedItemPosition() == 0 ? true : false
        );
    }


    public void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) this.getSystemService(this.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        }catch (Exception e){
            Log.e("TAG", "No esta activo el teclado");
        }
    }

    public void showDoctorHistory() {
        try {
        AdapterHistoryDoctor adapter = new AdapterHistoryDoctor(historyDoctorViewModel.getSpecificAppointmentDoctor(doctor.getProfessionalCard()), this);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);
            historyDoctorList.setLayoutManager(linearLayoutManager);
            historyDoctorList.setItemAnimator(new DefaultItemAnimator());
            historyDoctorList.setAdapter(adapter);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setEditDisabled() {
        consultingRoom.setEnabled(false);
        experience.setEnabled(false);
        speciality.setEnabled(false);
    }
}

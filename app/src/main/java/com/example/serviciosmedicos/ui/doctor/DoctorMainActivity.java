package com.example.serviciosmedicos.ui.doctor;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.serviciosmedicos.R;
import com.example.serviciosmedicos.ServicesApplication;
import com.example.serviciosmedicos.di.components.ApplicationComponent;
import com.example.serviciosmedicos.model.adapter.doctor.AdapterDoctorList;
import com.example.serviciosmedicos.model.doctor.Doctor;
import com.example.serviciosmedicos.ui.doctor.viewModel.DoctorHomeViewModel;
import com.github.clans.fab.FloatingActionMenu;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DoctorMainActivity extends AppCompatActivity{


    DoctorHomeViewModel doctorViewModel;

    @BindView(R.id.toolbar_doctor)
    Toolbar toolbar;

    @BindView(R.id.doctor_list)
    RecyclerView doctorList;

    @BindView(R.id.floating_button_doctor)
    FloatingActionMenu floatingActionMenu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor);
        ButterKnife.bind(this);
        setUpInjection();
        setUpToolbar();
        doctorViewModel = ViewModelProviders.of(this).get(DoctorHomeViewModel.class);
        showDoctors();

    }

    private void setUpInjection() {
        ((ServicesApplication) getApplication()).getApplicationComponent().inject(this);
    }

    public void setUpToolbar(){
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.doctor_title));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @OnClick(R.id.button_create_doctor)
    public void createDoctorForm(){
        Intent intent = new Intent(this, CreateDoctorActivity.class);
        startActivity(intent);
    }


    public void showDoctors() {

        doctorViewModel.getAllDoctor().observe(this, new Observer<List<Doctor>>() {
            @Override
            public void onChanged(@Nullable List<Doctor> doctors) {
                AdapterDoctorList adapter = new AdapterDoctorList(doctors,DoctorMainActivity.this);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(DoctorMainActivity.this, OrientationHelper.VERTICAL, false);
                doctorList.setLayoutManager(linearLayoutManager);
                doctorList.setItemAnimator(new DefaultItemAnimator());
                doctorList.setAdapter(adapter);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        showDoctors();
        floatingActionMenu.close(true);
    }

}

package com.example.serviciosmedicos.ui.doctor.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.serviciosmedicos.model.doctor.Doctor;
import com.example.serviciosmedicos.repository.DoctorRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class CreateDoctorViewModel extends AndroidViewModel {

    private DoctorRepository doctorRepository;

    @Inject
    public CreateDoctorViewModel(@NonNull Application application) {
        super(application);
        doctorRepository = new DoctorRepository(application);
    }

    public void insertDoctor(Doctor doctor){
        doctorRepository.insertDoctor(doctor);
    }
}

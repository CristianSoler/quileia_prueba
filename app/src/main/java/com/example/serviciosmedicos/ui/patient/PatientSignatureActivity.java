package com.example.serviciosmedicos.ui.patient;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.example.serviciosmedicos.R;
import com.example.serviciosmedicos.ServicesApplication;
import com.example.serviciosmedicos.model.appointment.MedicalAppointment;
import com.example.serviciosmedicos.ui.patient.viewModel.PatientSignatureViewModel;
import com.example.serviciosmedicos.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PatientSignatureActivity extends AppCompatActivity{

    PatientSignatureViewModel patientSignatureViewModel;
    SignatureCanvas canvas;
    MedicalAppointment medicalAppointment;

    @BindView(R.id.toolbar_signature)
    Toolbar toolbar;

    @BindView(R.id.canvas)
    LinearLayout canvasLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_signature);
        ButterKnife.bind(this);
        setUpInjection();
        medicalAppointment = (MedicalAppointment) getIntent().getSerializableExtra("medicalAppointment");
        patientSignatureViewModel = ViewModelProviders.of(this).get(PatientSignatureViewModel.class);
       canvas = new SignatureCanvas(this);
        canvasLayout.addView(canvas);
        setUpToolbar();
    }

    private void setUpInjection() {
        ((ServicesApplication) getApplication()).getApplicationComponent().inject(this);
    }

    public void setUpToolbar(){
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.signature));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public MedicalAppointment getAppointmentData() {
        MedicalAppointment medicalAppointmentData = new MedicalAppointment(
                medicalAppointment.getIdPatient(),
                medicalAppointment.getIdDoctor(),
                2,
                medicalAppointment.getMedicalAppointmentDate(),
                Double.valueOf(medicalAppointment.getFeeValue()),
                medicalAppointment.getArrivalDate(),
                Utils.getBytes(canvas.saveBitmap())
        );
        medicalAppointmentData.setId(medicalAppointment.getId());
        return medicalAppointmentData;
    }

    @OnClick(R.id.btn_register_signature)
    public void registerSignature(){
        patientSignatureViewModel.updateMedicalAppointment(getAppointmentData());
        finish();
    }


    public class SignatureCanvas extends View{
        Canvas canv;
        float x=0;
        float y=0;
        Path path;
        String action="accion";

        public SignatureCanvas(Context context) {
            super(context);
            path = new Path();
        }

        public void onDraw(Canvas canvas){
            canv = canvas;
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(5);
            paint.setColor(Color.BLUE);


            if (action.equals("down")){
                path.moveTo(x,y);
            }else if (action.equals("move")){
                path.lineTo(x,y);
            }
            canvas.drawPath(path, paint);


        }

        public Bitmap saveBitmap(){
            Bitmap  saveBitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvasBitmap = new Canvas(saveBitmap);
            canvasBitmap.setBitmap(saveBitmap);
            this.draw(canvasBitmap);
            return saveBitmap;
        }

        public boolean onTouchEvent(MotionEvent event){
            x = event.getX();
            y = event.getY();

            if(event.getAction() == MotionEvent.ACTION_DOWN){
                action = "down";
            }else if(event.getAction() == MotionEvent.ACTION_MOVE){
                action = "move";
            }

            invalidate();
            return true;
        }


    }
}

package com.example.serviciosmedicos.ui.patient;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.serviciosmedicos.R;
import com.example.serviciosmedicos.ServicesApplication;
import com.example.serviciosmedicos.model.adapter.patient.AdapterDoctorAppointment;
import com.example.serviciosmedicos.model.appointment.MedicalAppointment;
import com.example.serviciosmedicos.model.doctor.Doctor;
import com.example.serviciosmedicos.model.patient.Patient;
import com.example.serviciosmedicos.ui.patient.viewModel.PatientAppointmentViewModel;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PatientAppointmentActivity extends AppCompatActivity{


    PatientAppointmentViewModel patientAppointmentViewModel;
    Patient patient;
    boolean newPatient;
    TimePickerDialog timePicker;
    DatePickerDialog datePicker;
    Calendar appointmentDate;
    Activity contextBefore;
    int birthYear;
    int birthMonth;
    int birthDay;
    int birthHour;
    int birthMinute;

    @BindView(R.id.toolbar_patient_appointment)
    Toolbar toolbar;

    @BindView(R.id.appointment_doctor_list)
    RecyclerView appointmentDoctorList;

    @BindView(R.id.appointment_date_picked_text)
    TextView datePicked;

    @BindView(R.id.fee_value)
    TextInputEditText feeValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_appointment);
        ButterKnife.bind(this);
        setUpToolbar();
        setUpInjection();
        patient = (Patient) getIntent().getSerializableExtra("patient");
        newPatient = getIntent().getIntExtra("newPatient",-1) == 0;
        patientAppointmentViewModel = ViewModelProviders.of(this).get(PatientAppointmentViewModel.class);
        showDoctorsAppointment();
    }

    private void setUpInjection() {
        ((ServicesApplication) getApplication()).getApplicationComponent().inject(this);
    }

    public void setUpToolbar(){
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.patient_title));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @OnClick(R.id.btn_date_appointment)
    public void setAppointmentDate(){
        final Calendar calendar = Calendar.getInstance();
        appointmentDate = new GregorianCalendar();

        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        timePicker = new TimePickerDialog(
                PatientAppointmentActivity.this,
                R.style.Theme_AppCompat,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        birthHour = hourOfDay;
                        birthMinute = minute;

                        appointmentDate.set(birthYear, birthMonth, birthDay, birthHour, birthMinute);
                        datePicked.setText(birthYear + "/" + birthMonth + "/" + birthDay + "\n" + birthHour + ":" + birthMinute);
                    }
                },
                hour, minute, false);

        timePicker.show();


        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);

        datePicker = new DatePickerDialog(
                PatientAppointmentActivity.this,
                R.style.Theme_AppCompat,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        birthYear = year;
                        birthMonth = month;
                        birthDay = dayOfMonth;
                    }
                },
                year, month, day);
        datePicker.setButton(DialogInterface.BUTTON_NEGATIVE, "cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                timePicker.dismiss();
            }
        });
        datePicker.show();
    }

    public void showDoctorsAppointment(){
        patientAppointmentViewModel.getAllDoctors().observe(this, new Observer<List<Doctor>>() {
            @Override
            public void onChanged(@Nullable List<Doctor> doctors) {
                AdapterDoctorAppointment adapter = new AdapterDoctorAppointment(doctors,PatientAppointmentActivity.this);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PatientAppointmentActivity.this, OrientationHelper.VERTICAL, false);
                appointmentDoctorList.setLayoutManager(linearLayoutManager);
                appointmentDoctorList.setItemAnimator(new DefaultItemAnimator());
                appointmentDoctorList.setAdapter(adapter);
            }
        });
    }


    public MedicalAppointment getAppointmentData() {
        return new MedicalAppointment(
                patient.getId(),
                AdapterDoctorAppointment.doctorSelected.getProfessionalCard(),
                0,
                appointmentDate.getTime().getTime(),
                Double.valueOf(feeValue.getText().toString()),
                null,
                null
        );
    }

    @OnClick(R.id.btn_save_appointment)
    public void createAppointment(){
        try {
            if (newPatient){
                patientAppointmentViewModel.insertAppointment(getAppointmentData());
                System.out.println("id: "+ patient.getId() +"   appo: "+getAppointmentData().getIdPatient());
                this.finish();
            }else {
                patientAppointmentViewModel.insertAppointment(getAppointmentData());
                patientAppointmentViewModel.insertPatient(patient);
                System.out.println("id: "+ patient.getId() +"   appo: "+getAppointmentData().getIdPatient());
                CreatePatientActivity.fi.finish();
                this.finish();
            }
            finish();
        }catch (Exception e){
            Log.e("TAG", "No se ha seleccionado un doctor");
        }
    }
}

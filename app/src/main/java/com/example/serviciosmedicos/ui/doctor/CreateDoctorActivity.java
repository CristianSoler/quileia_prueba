package com.example.serviciosmedicos.ui.doctor;

import android.arch.lifecycle.ViewModelProviders;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.serviciosmedicos.R;
import com.example.serviciosmedicos.ServicesApplication;
import com.example.serviciosmedicos.model.doctor.Doctor;
import com.example.serviciosmedicos.ui.doctor.viewModel.CreateDoctorViewModel;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateDoctorActivity extends AppCompatActivity{

    private CreateDoctorViewModel createDoctorViewModel;

    @NotEmpty
    @BindView(R.id.toolbar_create_doctor)
    Toolbar toolbar;

    @BindView(R.id.profesional_card)
    TextInputEditText profesionalCard;

    @BindView(R.id.name_doctor)
    TextInputEditText name;

    @BindView(R.id.speciality)
    TextInputEditText speciality;

    @BindView(R.id.experience)
    TextInputEditText experience;

    @BindView(R.id.consulting_room)
    TextInputEditText consultingRoom;

    @BindView(R.id.domicile_switch)
    SwitchCompat domicile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_doctor);
        ButterKnife.bind(this);
        setUpInjection();
        setUpToolbar();

        createDoctorViewModel = ViewModelProviders.of(this).get(CreateDoctorViewModel.class);


    }

    private void setUpInjection() {
        ((ServicesApplication) getApplication()).getApplicationComponent().inject(this);
    }

    public void setUpToolbar(){
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.create_doctor_title));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @OnClick(R.id.btn_save_doctor)
    public void createDoctor(){

        createDoctorViewModel.insertDoctor(getDoctorData());
        finish();
    }

    public Doctor getDoctorData() {
        return new Doctor(
                profesionalCard.getText().toString(),
                name.getText().toString(),
                speciality.getText().toString(),
                Float.valueOf(experience.getText().toString()),
                consultingRoom.getText().toString(),
                domicile.isChecked()
        );

    }
}

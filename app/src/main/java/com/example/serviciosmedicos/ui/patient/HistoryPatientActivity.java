package com.example.serviciosmedicos.ui.patient;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.serviciosmedicos.R;
import com.example.serviciosmedicos.ServicesApplication;
import com.example.serviciosmedicos.model.adapter.patient.AdapterHistoryPatient;
import com.example.serviciosmedicos.model.appointment.MedicalAppointment;
import com.example.serviciosmedicos.model.patient.Patient;
import com.example.serviciosmedicos.ui.patient.viewModel.HistoryPatientViewModel;
import com.example.serviciosmedicos.utils.Utils;

import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HistoryPatientActivity extends AppCompatActivity{

    HistoryPatientViewModel historyPatientViewModel;
    Patient patient;

    @BindView(R.id.toolbar_history_patient)
    Toolbar toolbar;

    @BindView(R.id.history_patient_list)
    RecyclerView historyPatientList;

    @BindView(R.id.name_info)
    TextInputEditText name;

    @BindView(R.id.last_name_info)
    TextInputEditText lastName;

    @BindView(R.id.identification_info)
    TextInputEditText identification;

    @BindView(R.id.birthdate_info)
    TextInputEditText birthdate;

    @BindView(R.id.medication_info)
    Spinner medication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_patient);
        ButterKnife.bind(this);
        setUpInjection();
        setUpToolbar();
        patient = (Patient) getIntent().getSerializableExtra("patient");
        historyPatientViewModel = ViewModelProviders.of(this).get(HistoryPatientViewModel.class);
        showInfoPatient();
        showPatientHistory();
    }

    private void setUpInjection() {
        ((ServicesApplication) getApplication()).getApplicationComponent().inject(this);
    }

    public void setUpToolbar() {
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.patient_title));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
            case R.id.action_delete: {
                historyPatientViewModel.deletePatient(getPatientData());
                finish();
                break;
            }
            case R.id.action_save: {
                historyPatientViewModel.updatePatient(getPatientData());
                hideKeyboard();
                setEditDisabled();
                break;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.details_option_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void showInfoPatient() {
        name.setText(patient.getName());
        lastName.setText(patient.getLastName());
        identification.setText(patient.getId());
        birthdate.setText(Utils.getDateFormat(Utils.getDate(patient.getBirthdate())));
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.domicile_options, R.layout.spinner_center_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        medication.setAdapter(adapter);
        if (patient.isMedication()) {
            medication.setSelection(0);
        } else {
            medication.setSelection(1);
        }
    }

    public Patient getPatientData() {

        return new Patient(
                patient.getId(),
                name.getText().toString(),
                lastName.getText().toString(),
                patient.getBirthdate(),
                medication.getSelectedItemPosition() == 0 ? true : false
        );
    }

    public void setEditDisabled() {
        name.setEnabled(false);
        lastName.setEnabled(false);
    }

    public void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) this.getSystemService(this.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            Log.e("TAG", "No esta activo el teclado");
        }
    }

    public void showPatientHistory() {
        try {
            historyPatientViewModel.getAllMedicalAppointments(patient.getId()).observe(this, new Observer<List<MedicalAppointment>>() {
                @Override
                public void onChanged(@Nullable List<MedicalAppointment> medicalAppointmentList) {
                    AdapterHistoryPatient adapter = new AdapterHistoryPatient(medicalAppointmentList, HistoryPatientActivity.this);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HistoryPatientActivity.this, OrientationHelper.VERTICAL, false);
                    historyPatientList.setLayoutManager(linearLayoutManager);
                    historyPatientList.setItemAnimator(new DefaultItemAnimator());
                    historyPatientList.setAdapter(adapter);
                }
            });
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_edit_name)
    public void editName() {
        name.setEnabled(true);
    }

    @OnClick(R.id.btn_edit_last_name)
    public void editLastName() {
        lastName.setEnabled(true);
    }

    @OnClick(R.id.floating_button_appointment)
    public void createAppointment() {
        Intent intent = new Intent(this, PatientAppointmentActivity.class);
        intent.putExtra("patient", patient);
        intent.putExtra("newPatient", 0);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showInfoPatient();
        showPatientHistory();

    }
}

package com.example.serviciosmedicos.ui.patient;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.serviciosmedicos.R;
import com.example.serviciosmedicos.ServicesApplication;
import com.example.serviciosmedicos.model.patient.Patient;

import java.util.Calendar;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreatePatientActivity extends AppCompatActivity{

    TimePickerDialog timePicker;
    DatePickerDialog datePicker;
    Calendar birthDate;
    static Activity fi;
    int birthYear;
    int birthMonth;
    int birthDay;
    int birthHour;
    int birthMinute;

    @BindView(R.id.toolbar_create_patient)
    Toolbar toolbar;

    @BindView(R.id.date_picked_text)
    TextView datePicked;

    @BindView(R.id.name)
    TextInputEditText name;

    @BindView(R.id.last_name)
    TextInputEditText lastName;

    @BindView(R.id.identification)
    TextInputEditText identification;

    @BindView(R.id.medication_switch)
    SwitchCompat medication;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_patient);
        ButterKnife.bind(this);
        setUpInjection();
        setUpToolbar();
        fi = this;
    }

    private void setUpInjection() {
        ((ServicesApplication) getApplication()).getApplicationComponent().inject(this);
    }

    public void setUpToolbar() {
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.patient_title));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @OnClick(R.id.btn_date)
    public void setBirthDate() {
        final Calendar calendar = Calendar.getInstance();
        birthDate = new GregorianCalendar();

        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        timePicker = new TimePickerDialog(
                CreatePatientActivity.this,
                R.style.Theme_AppCompat,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        birthHour = hourOfDay;
                        birthMinute = minute;

                        birthDate.set(birthYear, birthMonth, birthDay, birthHour, birthMinute);
                        datePicked.setText(birthYear + "/" + birthMonth + "/" + birthDay + "\n" + birthHour + ":" + birthMinute);
                    }
                },
                hour, minute, false);

        timePicker.show();


        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);

        datePicker = new DatePickerDialog(
                CreatePatientActivity.this,
                R.style.Theme_AppCompat,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        birthYear = year;
                        birthMonth = month;
                        birthDay = dayOfMonth;
                    }
                },
                year, month, day);
        datePicker.setButton(DialogInterface.BUTTON_NEGATIVE, "cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                timePicker.dismiss();
            }
        });
        datePicker.show();
    }

    @OnClick(R.id.btn_save_patient)
    public void savePatient() {
        Intent intent = new Intent(this, PatientAppointmentActivity.class);
        intent.putExtra("patient",getPatientData());
        startActivity(intent);
    }



    public Patient getPatientData() {
        return new Patient(
            identification.getText().toString(),
                name.getText().toString(),
                lastName.getText().toString(),
                birthDate.getTime().getTime(),
                medication.isChecked()
        );
    }
}

package com.example.serviciosmedicos.ui.patient.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.serviciosmedicos.model.appointment.MedicalAppointment;
import com.example.serviciosmedicos.model.patient.Patient;
import com.example.serviciosmedicos.repository.MedicalAppointmentRepository;
import com.example.serviciosmedicos.repository.PatientRepository;

import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class HistoryPatientViewModel extends AndroidViewModel {

    private PatientRepository patientRepository;
    private MedicalAppointmentRepository medicalAppointmentRepository;
    private LiveData<List<MedicalAppointment>> allAppointments;

    @Inject
    public HistoryPatientViewModel(@NonNull Application application) {
        super(application);
        patientRepository = new PatientRepository(application);
        medicalAppointmentRepository = new MedicalAppointmentRepository(application);
        allAppointments = medicalAppointmentRepository.getAllMedicalAppointment();
    }

    public void updatePatient(Patient patient){
        patientRepository.updatePatient(patient);
    }

    public void deletePatient(Patient patient){
        deleteMedicalAppointmentFromSpecificPatient(patient.getId());
        patientRepository.deletePatient(patient);
    }

    public void deleteMedicalAppointmentFromSpecificPatient(String patientID){
        medicalAppointmentRepository.deleteMedicalAppointmentFromSpecificPatient(patientID);
    }

    public LiveData<List<MedicalAppointment>> getAllMedicalAppointments(String patientID) throws ExecutionException, InterruptedException {
        return medicalAppointmentRepository.getSpecificMedicalAppointmentWithPatientID(patientID);
    }

    public LiveData<List<MedicalAppointment>> getAllAppointments(){
        return allAppointments;
    }
}

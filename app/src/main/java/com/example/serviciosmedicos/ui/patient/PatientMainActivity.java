package com.example.serviciosmedicos.ui.patient;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.serviciosmedicos.R;
import com.example.serviciosmedicos.ServicesApplication;
import com.example.serviciosmedicos.model.adapter.patient.AdapterPatientList;
import com.example.serviciosmedicos.model.patient.Patient;
import com.example.serviciosmedicos.ui.patient.viewModel.PatientHomeViewModel;
import com.github.clans.fab.FloatingActionMenu;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PatientMainActivity extends AppCompatActivity{

    PatientHomeViewModel patientHomeViewModel;

    @BindView(R.id.toolbar_patient)
    Toolbar toolbar;

    @BindView(R.id.patient_list)
    RecyclerView patientList;

    @BindView(R.id.floating_action_menu_patient)
    FloatingActionMenu floatingActionMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_main);
        ButterKnife.bind(this);
        setUpInjection();
        setUpToolbar();
        patientHomeViewModel = ViewModelProviders.of(this).get(PatientHomeViewModel.class);
        showPatients();
    }

    private void setUpInjection() {
        ((ServicesApplication) getApplication()).getApplicationComponent().inject(this);
    }

    public void setUpToolbar(){
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.patient_title));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @OnClick(R.id.button_create_patient)
    public void createPatient(){
        Intent intent = new Intent(this, CreatePatientActivity.class);
        startActivity(intent);
    }

    public void showPatients() {
        patientHomeViewModel.getAllPatient().observe(this, new Observer<List<Patient>>() {
            @Override
            public void onChanged(@Nullable List<Patient> patients) {
                AdapterPatientList adapter = new AdapterPatientList(PatientMainActivity.this , patients);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PatientMainActivity.this, OrientationHelper.VERTICAL, false);
                patientList.setLayoutManager(linearLayoutManager);
                patientList.setItemAnimator(new DefaultItemAnimator());
                patientList.setAdapter(adapter);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        showPatients();
        floatingActionMenu.close(true);
    }

}

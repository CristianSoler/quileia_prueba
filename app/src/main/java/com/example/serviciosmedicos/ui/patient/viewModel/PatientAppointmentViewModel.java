package com.example.serviciosmedicos.ui.patient.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.serviciosmedicos.model.appointment.MedicalAppointment;
import com.example.serviciosmedicos.model.doctor.Doctor;
import com.example.serviciosmedicos.model.patient.Patient;
import com.example.serviciosmedicos.repository.DoctorRepository;
import com.example.serviciosmedicos.repository.MedicalAppointmentRepository;
import com.example.serviciosmedicos.repository.PatientRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PatientAppointmentViewModel extends AndroidViewModel {

    private PatientRepository patientRepository;
    private DoctorRepository doctorRepository;
    private MedicalAppointmentRepository medicalAppointmentRepository;
    private LiveData<List<Patient>> allPatients;
    private LiveData<List<Doctor>> allDoctors;

    @Inject
    public PatientAppointmentViewModel(@NonNull Application application) {
        super(application);
        patientRepository = new PatientRepository(application);
        doctorRepository = new DoctorRepository(application);
        allPatients = patientRepository.getAllPatient();
        allDoctors = doctorRepository.getAllDoctor();
        medicalAppointmentRepository = new MedicalAppointmentRepository(application);
    }

    public void insertPatient(Patient patient){
        patientRepository.insertPatient(patient);
    }

    public void insertAppointment(MedicalAppointment medicalAppointment){
        medicalAppointmentRepository.insertMedicalAppointmet(medicalAppointment);
    }

    public LiveData<List<Patient>> getAllPatients(){
        return allPatients;
    }

    public LiveData<List<Doctor>> getAllDoctors(){
        return allDoctors;
    }


}

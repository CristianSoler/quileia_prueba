package com.example.serviciosmedicos.ui.patient.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.example.serviciosmedicos.model.appointment.MedicalAppointment;
import com.example.serviciosmedicos.model.doctor.Doctor;
import com.example.serviciosmedicos.repository.DoctorRepository;
import com.example.serviciosmedicos.repository.MedicalAppointmentRepository;

import java.util.concurrent.ExecutionException;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PatientArrivalViewModel extends AndroidViewModel {

    private DoctorRepository doctorRepository;
    private MedicalAppointmentRepository medicalAppointmentRepository;

    @Inject
    public PatientArrivalViewModel(@NonNull Application application) {
        super(application);
        doctorRepository = new DoctorRepository(application);
        medicalAppointmentRepository = new MedicalAppointmentRepository(application);
    }

    public Doctor getSpecificDoctor(String doctorID) throws ExecutionException, InterruptedException {
     return doctorRepository.getSpecificDoctor(doctorID);
    }

    public void updateMedicalAppointment(MedicalAppointment medicalAppointment){
        medicalAppointmentRepository.updateMedicalAppointmet(medicalAppointment);
    }

    public MedicalAppointment getSpecificMedicalAppointment(Integer appointmentID) throws ExecutionException, InterruptedException {
        return medicalAppointmentRepository.getSpecificMedicalAppointment(appointmentID);
    }
}

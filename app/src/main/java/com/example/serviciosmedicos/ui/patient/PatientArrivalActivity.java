package com.example.serviciosmedicos.ui.patient;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.serviciosmedicos.R;
import com.example.serviciosmedicos.ServicesApplication;
import com.example.serviciosmedicos.model.appointment.MedicalAppointment;
import com.example.serviciosmedicos.model.doctor.Doctor;
import com.example.serviciosmedicos.ui.patient.viewModel.PatientArrivalViewModel;
import com.example.serviciosmedicos.utils.Utils;

import java.util.Calendar;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PatientArrivalActivity extends AppCompatActivity{

    PatientArrivalViewModel patientArrivalViewModel;
    MedicalAppointment medicalAppointment;
    Doctor doctor;

    @BindView(R.id.toolbar_patient_arrival)
    Toolbar toolbar;

    @BindView(R.id.signature_image)
    ImageView signatureImage;

    @BindView(R.id.layout_buttons)
    LinearLayout layoutButton;

    @BindView(R.id.layout_btn_register_arrived)
    LinearLayout layoutArrived;

    @BindView(R.id.layout_btn_register_attention)
    LinearLayout layoutAttention;

    @BindView(R.id.state_appointment_list_arrived)
    TextView appointmentState;

    @BindView(R.id.consulting_room_list_arrived)
    TextView consultingRoom;

    @BindView(R.id.medical_appointment_date_list_arrived)
    TextView appointmentDate;

    @BindView(R.id.fee_value_list_arrived)
    TextView feeValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_arrival);
        ButterKnife.bind(this);
        setUpInjection();
        setUpToolbar();
        medicalAppointment = (MedicalAppointment) getIntent().getSerializableExtra("medicalAppointment");
        patientArrivalViewModel = ViewModelProviders.of(this).get(PatientArrivalViewModel.class);
        try {
            System.out.println(medicalAppointment.getIdDoctor());
            doctor = patientArrivalViewModel.getSpecificDoctor(medicalAppointment.getIdDoctor());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        showDetailsAppointment();
    }

    private void setUpInjection() {
        ((ServicesApplication) getApplication()).getApplicationComponent().inject(this);
    }

    public void setUpToolbar() {
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.medical_appointment));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
            case R.id.action_report:{
                layoutButton.setVisibility(View.GONE);
                patientArrivalViewModel.updateMedicalAppointment(getReportedAppointmentData());
                break;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (medicalAppointment.getState() == 0) {
            getMenuInflater().inflate(R.menu.appointment_option_menu, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    public void isSinged(){
        System.out.println("hey");
        switch (medicalAppointment.getState()) {
            case 0: {
                appointmentState.setText(getString(R.string.no_arrived_yet));
                break;
            }
            case 1: {
                appointmentState.setText(getString(R.string.assist_text));
                layoutArrived.setVisibility(View.GONE);
                layoutAttention.setVisibility(View.VISIBLE);
                break;
            }
            case 2: {
                System.out.println("mememe");
                appointmentState.setText(getString(R.string.signed_text));
                showSignaureImage();
                layoutButton.setVisibility(View.GONE);
                break;
            }
            case 3:{
                layoutButton.setVisibility(View.GONE);
                appointmentState.setText(getString(R.string.no_assist_text));
                break;
            }
        }
    }

    public void showDetailsAppointment(){
        consultingRoom.setText(doctor.getConsultingRoom());
        appointmentDate.setText(Utils.getDateFormat(Utils.getDate(medicalAppointment.getMedicalAppointmentDate())));
        feeValue.setText(medicalAppointment.getFeeValue().toString());
        isSinged();
    }


    public MedicalAppointment getAppointmentData() {
        MedicalAppointment medicalAppointmentData = new MedicalAppointment(
                medicalAppointment.getIdPatient(),
                medicalAppointment.getIdDoctor(),
                1,
                medicalAppointment.getMedicalAppointmentDate(),
                medicalAppointment.getFeeValue(),
                Calendar.getInstance().getTime().getTime(),
                null
        );
        medicalAppointmentData.setId(medicalAppointment.getId());
        return medicalAppointmentData;
    }


    public MedicalAppointment getReportedAppointmentData(){
        MedicalAppointment medicalAppointmentData =  new MedicalAppointment(
                medicalAppointment.getIdPatient(),
                medicalAppointment.getIdDoctor(),
                3,
                medicalAppointment.getMedicalAppointmentDate(),
                Double.valueOf(medicalAppointment.getFeeValue()),
                Calendar.getInstance().getTime().getTime(),
                null
        );
        medicalAppointmentData.setId(medicalAppointment.getId());
        return medicalAppointmentData;
    }

    public void showSignaureImage(){
        signatureImage.setImageBitmap(Utils.getImage(medicalAppointment.getPatientSignature()));
    }

    @OnClick(R.id.btn_register_arrived)
    public void registerArrivedAppointment() {
        patientArrivalViewModel.updateMedicalAppointment(getAppointmentData());
        layoutArrived.setVisibility(View.GONE);
        layoutAttention.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.btn_register_attention)
    public void showPatientSignature(){
        Intent intent = new Intent(this, PatientSignatureActivity.class);
        intent.putExtra("medicalAppointment", medicalAppointment);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            medicalAppointment = patientArrivalViewModel.getSpecificMedicalAppointment(medicalAppointment.getId());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        showDetailsAppointment();
    }
}

package com.example.serviciosmedicos.ui.home;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.serviciosmedicos.R;
import com.example.serviciosmedicos.ServicesApplication;
import com.example.serviciosmedicos.repository.remote.Client;
import com.example.serviciosmedicos.ui.doctor.DoctorMainActivity;
import com.example.serviciosmedicos.ui.patient.PatientMainActivity;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setUpInjection();
        //TimerSyncTask task = new TimerSyncTask(this);
        //Timer timer = new Timer();
        //timer.schedule(task, 1000,10000);

    }

    private void setUpInjection() {
        ((ServicesApplication) getApplication()).getApplicationComponent().inject(this);
    }

    @OnClick(R.id.patient_button)
    public void patientButton(){
        Log.i("TAG","Paciente");
        Intent intent = new Intent(this, PatientMainActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.doctor_button)
    public void doctorButton(){
        Log.i("TAG","Doctor");
        Intent intent = new Intent(this, DoctorMainActivity.class);
        startActivity(intent);
    }

    class TimerSyncTask extends java.util.TimerTask{

        Client task;
        Context context;

        public TimerSyncTask(Context context) {
            this.context = context;
        }

        @Override
        public void run() {
            task = new Client(context);
            task.execute();
        }
    }
}

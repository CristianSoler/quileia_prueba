package com.example.serviciosmedicos.ui.patient.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.example.serviciosmedicos.model.appointment.MedicalAppointment;
import com.example.serviciosmedicos.repository.MedicalAppointmentRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PatientSignatureViewModel extends AndroidViewModel {

    private MedicalAppointmentRepository medicalAppointmentRepository;

    @Inject
    public PatientSignatureViewModel(@NonNull Application application) {
        super(application);
        medicalAppointmentRepository = new MedicalAppointmentRepository(application);
    }

    public void updateMedicalAppointment(MedicalAppointment medicalAppointment){
        medicalAppointmentRepository.updateMedicalAppointmet(medicalAppointment);
    }
}

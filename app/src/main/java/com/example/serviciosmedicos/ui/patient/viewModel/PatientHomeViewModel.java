package com.example.serviciosmedicos.ui.patient.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.serviciosmedicos.model.patient.Patient;
import com.example.serviciosmedicos.repository.PatientRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PatientHomeViewModel extends AndroidViewModel {

    private PatientRepository patientRepository;
    private LiveData<List<Patient>> allPatient;

    @Inject
    public PatientHomeViewModel(@NonNull Application application) {
        super(application);
        patientRepository = new PatientRepository(application);
        allPatient = patientRepository.getAllPatient();
    }

    public void insertDoctor(Patient patient){
        patientRepository.insertPatient(patient);
    }

    public void updateDoctor(Patient patient){
        patientRepository.updatePatient(patient);
    }

    public void deleteDoctor(Patient patient){
        patientRepository.deletePatient(patient);
    }

    public void deleteAllPatients(){
        patientRepository.deleteAllPatients();
    }

    public LiveData<List<Patient>> getAllPatient(){
        return allPatient;
    }
}

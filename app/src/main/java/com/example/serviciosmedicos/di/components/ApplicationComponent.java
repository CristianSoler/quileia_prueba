package com.example.serviciosmedicos.di.components;

import com.example.serviciosmedicos.ServicesApplication;
import com.example.serviciosmedicos.di.modules.ApplicationModule;
import com.example.serviciosmedicos.ui.doctor.CreateDoctorActivity;
import com.example.serviciosmedicos.ui.doctor.DoctorMainActivity;
import com.example.serviciosmedicos.ui.doctor.HistoryDoctorActivity;
import com.example.serviciosmedicos.ui.home.HomeActivity;
import com.example.serviciosmedicos.ui.patient.CreatePatientActivity;
import com.example.serviciosmedicos.ui.patient.HistoryPatientActivity;
import com.example.serviciosmedicos.ui.patient.PatientAppointmentActivity;
import com.example.serviciosmedicos.ui.patient.PatientArrivalActivity;
import com.example.serviciosmedicos.ui.patient.PatientMainActivity;
import com.example.serviciosmedicos.ui.patient.PatientSignatureActivity;
import com.example.serviciosmedicos.ui.patient.viewModel.PatientHomeViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(ServicesApplication servicesApplication);

    void inject(HomeActivity target);

    void inject(DoctorMainActivity target);

    void inject(CreateDoctorActivity target);

    void inject(HistoryDoctorActivity target);


    void inject(PatientMainActivity target);

    void inject(CreatePatientActivity target);

    void inject(PatientAppointmentActivity target);

    void inject(HistoryPatientActivity target);

    void inject(PatientArrivalActivity target);

    void inject(PatientSignatureActivity target);

}

package com.example.serviciosmedicos.di.modules;


import android.app.Service;

import com.example.serviciosmedicos.ServicesApplication;
import com.example.serviciosmedicos.repository.DoctorRepository;
import com.example.serviciosmedicos.repository.MedicalAppointmentRepository;
import com.example.serviciosmedicos.repository.PatientRepository;
import com.example.serviciosmedicos.ui.doctor.viewModel.CreateDoctorViewModel;
import com.example.serviciosmedicos.ui.doctor.viewModel.DoctorHomeViewModel;
import com.example.serviciosmedicos.ui.doctor.viewModel.HistoryDoctorViewModel;
import com.example.serviciosmedicos.ui.patient.viewModel.HistoryPatientViewModel;
import com.example.serviciosmedicos.ui.patient.viewModel.PatientAppointmentViewModel;
import com.example.serviciosmedicos.ui.patient.viewModel.PatientArrivalViewModel;
import com.example.serviciosmedicos.ui.patient.viewModel.PatientHomeViewModel;
import com.example.serviciosmedicos.ui.patient.viewModel.PatientSignatureViewModel;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final ServicesApplication servicesApplication;

    public ApplicationModule(ServicesApplication servicesApplication) {
        this.servicesApplication = servicesApplication;
    }

    @Provides
    @Singleton
    DoctorHomeViewModel providerDoctorHomeViewModel(DoctorHomeViewModel doctorHomeViewModel) {
        return doctorHomeViewModel;
    }

    @Provides
    @Singleton
    CreateDoctorViewModel providerCreateDoctorViewModel(CreateDoctorViewModel createDoctorViewModel){
        return createDoctorViewModel;
    }

    @Provides
    @Singleton
    HistoryDoctorViewModel providerHistoryDoctorViewModel(HistoryDoctorViewModel historyDoctorViewModel){
        return historyDoctorViewModel;
    }

    @Provides
    @Singleton
    PatientHomeViewModel providerPatientHomeViewModel(PatientHomeViewModel patientHomeViewModel){
        return patientHomeViewModel;
    }

    @Provides
    @Singleton
    PatientAppointmentViewModel providerPatientAppointmentViewModel(PatientAppointmentViewModel patientAppointmentViewModel){
        return patientAppointmentViewModel;
    }

    @Provides
    @Singleton
    HistoryPatientViewModel providerHistoryPatientViewModel(HistoryPatientViewModel historyPatientViewModel){
        return historyPatientViewModel;
    }

    @Provides
    @Singleton
    PatientArrivalViewModel providerPatientArrivalViewModel(PatientArrivalViewModel patientArrivalViewModel){
        return patientArrivalViewModel;
    }

    @Provides
    @Singleton
    PatientSignatureViewModel providerPatientSignatureViewModel(PatientSignatureViewModel patientSignatureViewModel){
        return patientSignatureViewModel;
    }

    @Provides
    @Singleton
    DoctorRepository providerDoctorRepository(DoctorRepository doctorRepository){
        return doctorRepository;
    }

    @Provides
    @Singleton
    MedicalAppointmentRepository providerMedicalAppointmentRepository(MedicalAppointmentRepository medicalAppointmentRepository){
        return medicalAppointmentRepository;
    }

    @Provides
    @Singleton
    PatientRepository providerPatientRepository(PatientRepository patientRepository){
        return patientRepository;
    }
}

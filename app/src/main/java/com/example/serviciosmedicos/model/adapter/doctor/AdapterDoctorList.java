package com.example.serviciosmedicos.model.adapter.doctor;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.serviciosmedicos.R;
import com.example.serviciosmedicos.model.doctor.Doctor;
import com.example.serviciosmedicos.ui.doctor.HistoryDoctorActivity;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.serviciosmedicos.R.color.colorAccent;

public class AdapterDoctorList extends RecyclerView.Adapter<AdapterDoctorList.DoctorHolder> {

    private List<Doctor> doctorList;
    private Context context;

    public AdapterDoctorList(List<Doctor> doctorList, Context context) {
        this.doctorList = new ArrayList<>();
        this.context = context;
        this.doctorList = doctorList;
    }

    public static class DoctorHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.profesional_card_list)
        TextView pcList;

        @BindView(R.id.consulting_room_list)
        TextView crList;

        @BindView(R.id.experience_list)
        TextView expList;

        @BindView(R.id.speciality_list)
        TextView speList;

        @BindView(R.id.domicile_list)
        TextView domList;

        @BindView(R.id.doctor_linear_layout)
        LinearLayout doctor;

        public DoctorHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public AdapterDoctorList.DoctorHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.doctor_list_element, viewGroup, false);
        return new DoctorHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final DoctorHolder doctorHolder, final int i) {
        if (doctorList.size() - 1 == i) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) doctorHolder.doctor.getLayoutParams();
            params.bottomMargin = 350;

        }
        doctorHolder.pcList.setText(doctorList.get(i).getProfessionalCard());
        doctorHolder.crList.setText(doctorList.get(i).getConsultingRoom());
        doctorHolder.expList.setText(String.valueOf(doctorList.get(i).getExperience()));
        doctorHolder.speList.setText(doctorList.get(i).getSpeciality());
        if (doctorList.get(i).isDomicile()) {
            doctorHolder.domList.setText(context.getString(R.string.yes));
        } else {
            doctorHolder.domList.setText(context.getString(R.string.no));
        }

        doctorHolder.doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHistoryDoctor(doctorList.get(i));
            }
        });

    }

    public void showHistoryDoctor(Doctor doctor) {
        Intent intent = new Intent(context, HistoryDoctorActivity.class);
        intent.putExtra("Doctor", doctor);
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return doctorList.size();
    }
}

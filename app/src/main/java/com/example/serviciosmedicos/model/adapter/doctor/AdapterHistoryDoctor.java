package com.example.serviciosmedicos.model.adapter.doctor;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.serviciosmedicos.R;
import com.example.serviciosmedicos.model.appointment.MedicalAppointment;
import com.example.serviciosmedicos.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterHistoryDoctor extends RecyclerView.Adapter<AdapterHistoryDoctor.HistoryDoctorHolder> {

    List<MedicalAppointment> historyList;
    Context context;

    public AdapterHistoryDoctor(List<MedicalAppointment> historyList, Context context) {
        this.historyList = new ArrayList<>();
        this.historyList = historyList;
        this.context = context;
    }

    public static class HistoryDoctorHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.fee_value_historyd_list)
        TextView feeValue;

        @BindView(R.id.state_historyd_list)
        TextView state;

        @BindView(R.id.appointment_date_historyd_list)
        TextView appointmentDate;

        @BindView(R.id.appointmentd_linear_layout)
        LinearLayout appointment;

        public HistoryDoctorHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public HistoryDoctorHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.history_doctor_element, viewGroup, false);
        return new HistoryDoctorHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryDoctorHolder historyDoctorHolder, int i) {
        if (historyList.size() - 1 == i) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) historyDoctorHolder.appointment.getLayoutParams();
            params.bottomMargin = 350;

        }

        historyDoctorHolder.appointmentDate.setText(Utils.getDateFormat(Utils.getDate(historyList.get(i).getMedicalAppointmentDate())));
        historyDoctorHolder.feeValue.setText(historyList.get(i).getFeeValue().toString());

        switch (historyList.get(i).getState()){
            case 0:{
                historyDoctorHolder.state.setText(R.string.no_arrived_yet);
                break;
            }
            case 1:{
                historyDoctorHolder.state.setText(R.string.assist_text);
                break;
            }
            case 2:{
                historyDoctorHolder.state.setText(R.string.signed_text);
                break;
            }
            case 3:{
                historyDoctorHolder.state.setText(R.string.no_assist_text);
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }

}

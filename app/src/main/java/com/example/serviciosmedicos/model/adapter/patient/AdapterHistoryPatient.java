package com.example.serviciosmedicos.model.adapter.patient;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.serviciosmedicos.R;
import com.example.serviciosmedicos.model.appointment.MedicalAppointment;
import com.example.serviciosmedicos.ui.patient.PatientArrivalActivity;
import com.example.serviciosmedicos.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterHistoryPatient extends RecyclerView.Adapter<AdapterHistoryPatient.HistoryPatientHolder> {

    List<MedicalAppointment> historyList;
    Context context;

    public AdapterHistoryPatient(List<MedicalAppointment> historyList, Context context) {
        this.historyList = new ArrayList<>();
        this.historyList = historyList;
        this.context = context;
    }

    public static class HistoryPatientHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.fee_value_history_list)
        TextView feeValue;

        @BindView(R.id.state_history_list)
        TextView state;

        @BindView(R.id.appointment_date_history_list)
        TextView appointmentDate;

        @BindView(R.id.appointment_linear_layout)
        LinearLayout appointment;

        public HistoryPatientHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public HistoryPatientHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.history_patient_element, viewGroup, false);
        return new HistoryPatientHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final HistoryPatientHolder historyPatientHolder, final int i) {
        if (historyList.size() - 1 == i) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) historyPatientHolder.appointment.getLayoutParams();
            params.bottomMargin = 350;

        }

        historyPatientHolder.appointmentDate.setText(Utils.getDateFormat(Utils.getDate(historyList.get(i).getMedicalAppointmentDate())));
        historyPatientHolder.feeValue.setText(historyList.get(i).getIdPatient());
        switch (historyList.get(i).getState()){
            case 0:{
                historyPatientHolder.state.setText(R.string.no_arrived_yet);
                break;
            }
            case 1:{
                historyPatientHolder.state.setText(R.string.assist_text);
                break;
            }
            case 2:{
                historyPatientHolder.state.setText(R.string.signed_text);
                break;
            }
            case 3:{
                historyPatientHolder.state.setText(R.string.no_assist_text);
                break;
            }
        }

        historyPatientHolder.appointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPatientsArrival(historyList.get(i));
            }
        });
    }

    public void showPatientsArrival(MedicalAppointment medicalAppointment){
        Intent intent = new Intent(context, PatientArrivalActivity.class);
        intent.putExtra("medicalAppointment",medicalAppointment);
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }

}

package com.example.serviciosmedicos.model.adapter.patient;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.serviciosmedicos.R;
import com.example.serviciosmedicos.model.patient.Patient;
import com.example.serviciosmedicos.ui.patient.HistoryPatientActivity;
import com.example.serviciosmedicos.utils.Utils;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterPatientList extends RecyclerView.Adapter<AdapterPatientList.PatientHolder> {

    Context context;
    List<Patient> patientList;

    public AdapterPatientList(Context context, List<Patient> patientList) {
        this.patientList = new ArrayList<>();
        this.context = context;
        this.patientList = patientList;
    }

    public static class PatientHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.name_list)
        TextView name;

        @BindView(R.id.birthdate_list)
        TextView birthDate;

        @BindView(R.id.identification_list)
        TextView identification;

        @BindView(R.id.medication_list)
        TextView medication;

        @BindView(R.id.patient_linear_layout)
        LinearLayout patient;

        public PatientHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public PatientHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(context).inflate(R.layout.patient_list_element,viewGroup,false);
        return new PatientHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PatientHolder patientHolder, final int i) {
        if (patientList.size() - 1 == i) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) patientHolder.patient.getLayoutParams();
            params.bottomMargin = 350;

        }
        patientHolder.name.setText(patientList.get(i).getName()+" "+patientList.get(i).getLastName());
        patientHolder.birthDate.setText(Utils.getDateFormat(Utils.getDate(patientList.get(i).getBirthdate())));
        patientHolder.identification.setText(patientList.get(i).getId());
        if (patientList.get(i).isMedication()) {
            patientHolder.medication.setText(context.getString(R.string.yes));
        }else{
            patientHolder.medication.setText(context.getString(R.string.no));
        }

        patientHolder.patient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHistoryPatient(patientList.get(i));
            }
        });
    }

    public void showHistoryPatient(Patient patient){
        Intent intent = new Intent(context, HistoryPatientActivity.class);
        intent.putExtra("patient", patient);
        context.startActivity(intent);
    }



    @Override
    public int getItemCount() {
        return patientList.size();
    }

}

package com.example.serviciosmedicos.model.appointment;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "medical_appointment_table")
public class MedicalAppointment implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private Integer id;

    private String idPatient;

    private String idDoctor;

    private int state;

    private Long medicalAppointmentDate;

    private Double feeValue;

    private Long arrivalDate;

    private byte[] patientSignature;

    public MedicalAppointment(String idPatient, String idDoctor, int state, Long medicalAppointmentDate, Double feeValue, Long arrivalDate, byte[] patientSignature) {
        this.idPatient = idPatient;
        this.idDoctor = idDoctor;
        this.state = state;
        this.medicalAppointmentDate = medicalAppointmentDate;
        this.feeValue = feeValue;
        this.arrivalDate = arrivalDate;
        this.patientSignature = patientSignature;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Long arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(String idPatient) {
        this.idPatient = idPatient;
    }

    public String getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(String idDoctor) {
        this.idDoctor = idDoctor;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Long getMedicalAppointmentDate() {
        return medicalAppointmentDate;
    }

    public void setMedicalAppointmentDate(Long medicalAppointmentDate) {
        this.medicalAppointmentDate = medicalAppointmentDate;
    }

    public Double getFeeValue() {
        return feeValue;
    }

    public void setFeeValue(Double feeValue) {
        this.feeValue = feeValue;
    }

    public byte[] getPatientSignature() {
        return patientSignature;
    }

    public void setPatientSignature(byte[] patientSignature) {
        this.patientSignature = patientSignature;
    }
}

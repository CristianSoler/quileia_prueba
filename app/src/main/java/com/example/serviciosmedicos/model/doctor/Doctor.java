package com.example.serviciosmedicos.model.doctor;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;

@Entity(tableName = "doctor_table")
public class Doctor implements Serializable {

    @NonNull
    @PrimaryKey(autoGenerate = false)
    private String professionalCard;

    private String name;

    private String speciality;

    private Float experience;

    private String consultingRoom;

    private boolean domicile;

    public Doctor(String professionalCard,String name, String speciality, Float experience, String consultingRoom, boolean domicile) {
        this.professionalCard = professionalCard;
        this.name = name;
        this.speciality = speciality;
        this.experience = experience;
        this.consultingRoom = consultingRoom;
        this.domicile = domicile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfessionalCard() {
        return professionalCard;
    }

    public void setProfessionalCard(String professionalCard) {
        this.professionalCard = professionalCard;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public Float getExperience() {
        return experience;
    }

    public void setExperience(Float experience) {
        this.experience = experience;
    }

    public String getConsultingRoom() {
        return consultingRoom;
    }

    public void setConsultingRoom(String consultingRoom) {
        this.consultingRoom = consultingRoom;
    }

    public boolean isDomicile() {
        return domicile;
    }

    public void setDomicile(boolean domicile) {
        this.domicile = domicile;
    }
}

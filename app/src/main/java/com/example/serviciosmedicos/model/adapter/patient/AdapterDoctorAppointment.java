package com.example.serviciosmedicos.model.adapter.patient;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.serviciosmedicos.R;
import com.example.serviciosmedicos.model.doctor.Doctor;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterDoctorAppointment extends RecyclerView.Adapter<AdapterDoctorAppointment.DoctorAppointmentHolder> {

    private List<Doctor> doctorList;
    private Context context;
    private DoctorAppointmentHolder target;
    public static Doctor doctorSelected;


    public AdapterDoctorAppointment(List<Doctor> doctorList, Context context) {
        this.doctorList = new ArrayList<>();
        this.context = context;
        this.doctorList = doctorList;
    }

    public static class DoctorAppointmentHolder extends RecyclerView.ViewHolder{


        @BindView(R.id.doctor_appointment_linear_layout)
        LinearLayout doctorAppointment;

        @BindView(R.id.speciality_appointment_list)
        TextView specialityAppointment;

        @BindView(R.id.domicile_appointment_list)
        TextView domicileAppointment;

        @BindView(R.id.consulting_room_appointment_list)
        TextView consultingRoomAppointment;

        public DoctorAppointmentHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public DoctorAppointmentHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.appointment_doctor_list_element, viewGroup, false);
        return new DoctorAppointmentHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final DoctorAppointmentHolder doctorAppointmentHolder, final int i) {
        doctorAppointmentHolder.specialityAppointment.setText(doctorList.get(i).getSpeciality());
        if (doctorList.get(i).isDomicile()) {
            doctorAppointmentHolder.domicileAppointment.setText(context.getString(R.string.domicile_attend));
        } else {
            doctorAppointmentHolder.domicileAppointment.setText(context.getString(R.string.no_domicile_attend));
        }
        doctorAppointmentHolder.consultingRoomAppointment.setText(doctorList.get(i).getConsultingRoom());

        doctorAppointmentHolder.doctorAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (target != null){
                    target.doctorAppointment.setBackgroundResource(R.color.white_gray);
                    doctorAppointmentHolder.doctorAppointment.setBackgroundColor(Color.GREEN);
                    target = doctorAppointmentHolder;
                    doctorSelected = doctorList.get(i);
                }else {
                    doctorAppointmentHolder.doctorAppointment.setBackgroundColor(Color.GREEN);
                    target = doctorAppointmentHolder;
                    doctorSelected = doctorList.get(i);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return doctorList.size();
    }

}

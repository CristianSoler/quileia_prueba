package com.example.serviciosmedicos;


import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;


import com.example.serviciosmedicos.ui.home.HomeActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class CreateDoctorEspressoTest {


    @Rule
    public ActivityTestRule<HomeActivity> activityTestRule = new ActivityTestRule<>(HomeActivity.class);

    @Test
    public void editableTextView(){
        onView(withId(R.id.doctor_button)).perform(click());

        onView(withId(R.id.floating_button_doctor)).perform(click());

        onView(withId(R.id.button_create_doctor)).perform(click());

        onView(withId(R.id.name_doctor))
                .perform(typeText("Cristian"), ViewActions.closeSoftKeyboard());

        onView(withId(R.id.profesional_card))
                .perform(typeText("2KEY4"), ViewActions.closeSoftKeyboard());

        onView(withId(R.id.speciality))
                .perform(typeText("Nutricionista"), ViewActions.closeSoftKeyboard());

        onView(withId(R.id.experience))
                .perform(typeText("3.5"), ViewActions.closeSoftKeyboard());

        onView(withId(R.id.consulting_room))
                .perform(typeText("Calle 45 #11-41"), ViewActions.closeSoftKeyboard());

        onView(withId(R.id.btn_save_doctor))
                .perform(click());
    }
}
